import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import * as VueGoogleMaps from "vue2-google-maps";

Vue.use(VueAxios, axios);
Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyCW30OWS0Kc4lbgvl5V6XSBfSQVvLjPzEY",
    libraries: "places",
    region: "uk,en"
  }
});

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
