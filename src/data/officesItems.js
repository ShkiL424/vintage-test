export const officeItems = [
  {id: 1,
    city: 'Kyiv',
    title: 'Global Message Services Ukraine LLC',
    address: ['Kuiv', 'Stepan Bandera','33', '02066 Ukraine'],
    center: {lat: 50.431782, lng: 30.516382}
  },
  {id: 2,
    city: 'Kharkiv',
    title: 'Message Services Ukraine ',
    address: ['Kharkiv', ' Bandera','35', '23411 Ukraine'],
    center: {lat: 49.988358, lng: 36.232845}
  },
  {id: 3,
    city: 'Dnepr',
    title: 'Global Message Services  LLC',
    address: ['Dnepr', 'Stepan ','19', '98554 Ukraine'],
    center: {lat: 48.4500000, lng: 34.9833300}
  },
  {id: 4,
    city: 'Odessa',
    title: 'Message  Ukraine LLC',
    address: ['Odessa', 'Stepan Bandera','44', '09221 Ukraine'],
    center: {lat: 46.469391, lng: 30.740883}
  }
]
